Lolnero is a fork of [Wownero][1] with a linear emission and a [SHA-3][2] PoW.

There is no premine and no dev tax.


Why
---

The goal of Lolnero is to replace `C/C++` with a safer language, and to not hardfork.


Specifications
--------------

* Proof of Work: SHA-3
* Max supply: ∞
* Block reward: 300
* Block time: 5 minutes
* Block size limit: max(128k, Block height) bytes
* Confidential transaction: Bulletproofs
* Ring signature: CLSAG
* Ring size: 32

## [source code](https://gitlab.com/fuwa/lolnero)

## [[build]]#

## [[tor]]#

## [[Android Guide]]#

## [[resources]]#

[1]:https://wownero.org/
[2]:https://en.wikipedia.org/wiki/SHA-3

