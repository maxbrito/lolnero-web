# Full anonymity with tor

## Use tor for everything

```
lolnerod \
--proxy public,127.0.0.1:9050 \
--proxy tor,127.0.0.1:9050
```

## Use tor only for hidden services

```
lolnerod \
--proxy tor,127.0.0.1:9050
```

## Use tor for clear net, and do not connect to hidden services

```
lolnerod \
--proxy public,127.0.0.1:9050
```


## Allow incoming connections through a hidden service (requires manual set up of tor)


```
lolnerod \
--proxy tor,127.0.0.1:9050 \
--anonymous-inbound HIDDEN_SERVICE_ID.onion:45678,127.0.0.1:44444
```

Here port `44444` is an arbitrary port, used only to accept incoming connections from the hidden service.

An example snippet of the tor config file `torrc` that can accompany this set up is the following.

```
HiddenServiceDir /var/lib/tor/onion/lolnero
HiddenServicePort 45678 44444
```
