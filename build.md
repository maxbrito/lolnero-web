## Arch

```
sudo pacman -S base-devel cmake git
sudo pacman -S boost readline libsodium openssl rapidjson

git clone https://gitlab.com/fuwa/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Debian / Ubuntu 

* Debian 11 "Bullseye" (Debian testing)
* Ubuntu 21.04 (Hirsute Hippo)

```
sudo apt install build-essential cmake git -y

sudo apt install \
libboost-dev \
libboost-program-options-dev \
libboost-serialization-dev \
libboost-system-dev \
-y

sudo apt install \
libreadline6-dev \
libsodium-dev \
libssl-dev \
rapidjson-dev \
-y

git clone https://gitlab.com/fuwa/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Gentoo

```
sudo emerge \
dev-util/cmake \
dev-vcs/git

sudo emerge \
dev-libs/boost \
sys-libs/ncurses \
dev-libs/libsodium \
dev-libs/openssl \
dev-libs/rapidjson

git clone https://gitlab.com/fuwa/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## NixOS

```
git clone https://gitlab.com/fuwa/lolnero.git

nix-shell lolnero/etc/nix/env.nix

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.
