# Lolnero seed generator

## Download Lolnero seed generator

[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'>](https://play.google.com/store/apps/details?id=org.lolnero.lolnero_seed)

## Generate a new seed

Starting the app will generate a new seed.
