## Usage

The simplest set up of a full node and a new wallet on Android requires the following steps:

1. Install [[Android Node]]#
2. Install [[Android Seed Generator]]#
3. Install [[Android Wallet]]# and send a seed to it

Detailed information on how to send a seed to a wallet is in the [[Android Wallet]] guide.

## [[Android Links]]#
