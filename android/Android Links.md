Node
----

[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'>](https://play.google.com/store/apps/details?id=org.lolnero.node)


### source code: <https://gitlab.com/fuwa/lolnode>

Seed generator
--------------
[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'>](https://play.google.com/store/apps/details?id=org.lolnero.lolnero_seed)


### source code: <https://gitlab.com/fuwa/lolnero-seed>

Wallet
------
[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'>](https://play.google.com/store/apps/details?id=org.lolnero.lolnero_wallet)


### source code: <https://gitlab.com/fuwa/lolnero-wallet>

