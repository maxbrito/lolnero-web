# Lolnero wallet

## Download Lolnero wallet

[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'>](https://play.google.com/store/apps/details?id=org.lolnero.lolnero_wallet)

## Create a new wallet

You need to send a seed to it by using [[Android Seed Generator]]#.

1. Make sure your [[Android Node]]# is synced.
2. Launch the [[Android Seed Generator]]#, press the "Send" button
3. Your phone will prompt you with a list of apps installed on your phone
4. Select "L wallet"

Your wallet should start to scan the block chain, and show a balance of 0, since this is a new wallet.

## Reset a wallet

* Uninstalling and reinstalling [[Android Wallet]]# will set up a new wallet
* Settings -> Apps & notifcations -> "L wallet" -> Storage & cache -> Clear storage

## Backup your seed

In step 4 of "Create a new wallet", instead of sending to "L wallet", send it to another note taking app, like [Google Keep](https://keep.google.com/).

## Restore from a seed

1. Make sure [[Android Wallet]]# doesn't contain an existing wallet, if it does, "Reset a wallet".
2. Long press and select your seed, possibly from a note taking app
3. Press "Share"
4. Continue from step 3 of "Create a new wallet"

## Show the seed for an existing wallet

1. Swipe left twice in [[Android Wallet]]#
2. In the log view, long press the rocket icon
3. Type "seed" in the terminal view
4. Take a screenshot or write it down
5. Type "exit"
